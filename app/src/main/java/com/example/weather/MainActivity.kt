package com.example.weather

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.NumberPicker
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.weather.services.WeatherResponse
import com.example.weather.services.WeatherService
import com.google.firebase.iid.FirebaseInstanceId
import org.json.JSONObject
import java.net.URL
import org.json.JSONException
import java.lang.NullPointerException

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val cityInput = findViewById<TextView>(R.id.city_input)
        registerDoneTypingEvents(cityInput)
        cityInput.text = AppSettings.CITY;
        getWeatherForCity(cityInput)
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener{
            it.result?.token?.let{
                    println("El token del dispositivo es: ${it}")
                }
        }
    }

    private fun registerDoneTypingEvents(cityInput: TextView) {
        //https://stackoverflow.com/questions/8063439/android-edittext-finished-typing-event
        cityInput.setOnFocusChangeListener { view, hasFocus ->
            if (!hasFocus) {
                getWeatherForCity(cityInput)
            }
        }
        cityInput.setOnEditorActionListener(
            OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                    || actionId == EditorInfo.IME_ACTION_DONE
                    || event != null && event.action == KeyEvent.ACTION_DOWN && event.keyCode == KeyEvent.KEYCODE_ENTER
                ) {
                    if (event == null || !event.isShiftPressed) {
                        getWeatherForCity(cityInput)
                        return@OnEditorActionListener true // consume.
                    }
                }
                false
            }
        )
    }

    private fun updateView(weatherResponse: WeatherResponse){
        findViewById<TextView>(R.id.rain_chance).text = weatherResponse.rain + "% lluvia"
        findViewById<TextView>(R.id.temperature).text = weatherResponse.tempInC + "°C"
        val imageAsset = ImagesProvider.getImage(weatherResponse.weather)
        findViewById<ImageView>(R.id.background).setImageDrawable(resources.getDrawable(imageAsset, applicationContext.theme))
    }

    private fun getWeatherForCity(cityInput: TextView) {
        val service = WeatherService()
        val weatherResponse = service.getWeatherForCity(cityInput.text.toString())
        if (weatherResponse.error == null) {
            updateView(weatherResponse)
        } else {
            showErrorDialog(
                if (weatherResponse.error is NullPointerException) "No se encontro la ciudad" else "No fue posible conectarse al servidor, por favor reintente más tarde",
                "Error"
            )
        }
        hideKeyboard()
    }

    private fun hideKeyboard() {
        currentFocus?.let { v ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(v.windowToken, 0)
        }
    }

    //Parameter needs to be there so that we can reference the method from the layout
    // and thus avoid boilerplate registering the method
    @Suppress("UNUSED_PARAMETER")
    fun reloadWeather(view: View) {
        getWeatherForCity(findViewById(R.id.city_input))
    }

    private fun showErrorDialog(message: String, title: String) {
        val builder: AlertDialog.Builder = this.let {
            AlertDialog.Builder(it)
        }
        builder.setMessage(message)
            .setTitle(title)
        builder.create().show()
    }

    @Suppress("UNUSED_PARAMETER")
    fun showConfigurationDialog(view: View) {
        val builder: AlertDialog.Builder = this.let {
            AlertDialog.Builder(it)
        }
        val inflater = this.layoutInflater
        val numberPickerView = inflater.inflate(R.layout.config_dialog, null)
        val numberPicker: NumberPicker = numberPickerView.findViewById(R.id.numberPicker);
        numberPicker.maxValue = 100
        numberPicker.minValue = 0
        numberPicker.value = this.getSharedPreferences(AppSettings.RAIN_CHANCE_THRESHOLD_KEY, Context.MODE_PRIVATE)
            .getInt(AppSettings.RAIN_CHANCE_THRESHOLD_KEY, AppSettings.RAIN_CHANCE_THRESHOLD_DEFAULT)
        builder.setTitle("Configuración")
            .setMessage("Elija % de probabilidad de lluvia para recibir notificaciones")
            .setView(numberPickerView)
            .setPositiveButton("Guardar") { dialog, id ->
                with (this.getSharedPreferences(AppSettings.RAIN_CHANCE_THRESHOLD_KEY, Context.MODE_PRIVATE).edit()) {
                    putInt(AppSettings.RAIN_CHANCE_THRESHOLD_KEY, numberPicker.value)
                    apply()
                }
                dialog.dismiss()
            }
            .setNegativeButton("Cancelar") { dialog, _ -> dialog.cancel()}
        builder.create().show()
    }
}
