package com.example.weather

class AppSettings {
    companion object {
        const val CITY: String = "Buenos Aires"
        const val API_URL: String = "https://magios-weather-server.herokuapp.com/"
        const val RAIN_CHANCE_THRESHOLD_KEY = "rainChanceThreshold"
        const val RAIN_CHANCE_THRESHOLD_DEFAULT = 60
    }
}
