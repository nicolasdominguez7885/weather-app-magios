package com.example.weather

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService: FirebaseMessagingService() {
    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        FirebaseMessaging.getInstance().subscribeToTopic("weather")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val title = remoteMessage.notification?.title
        val body = remoteMessage.notification?.body
        val threshold = applicationContext.getSharedPreferences(AppSettings.RAIN_CHANCE_THRESHOLD_KEY, Context.MODE_PRIVATE)
            .getInt(AppSettings.RAIN_CHANCE_THRESHOLD_KEY, AppSettings.RAIN_CHANCE_THRESHOLD_DEFAULT)
        if (body != null && body.toInt() >= threshold) {
            showNotification(title)
        }
    }

    private fun showNotification(title: String?){
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val cId = "fcm_default_channel"
        val dSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val nBuilder = NotificationCompat.Builder(this, cId)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(title)
            .setContentText("¡Si vas a salir no te olvides el paraguas!")
            .setSound(dSoundUri)
            .setContentIntent(pendingIntent)
            .setPriority(Notification.PRIORITY_HIGH)
        val nManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        //Since android Oreo
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val oChannel = NotificationChannel(cId, "Customer", NotificationManager.IMPORTANCE_HIGH)
            nManager.createNotificationChannel(oChannel)
        }
        nManager.notify(0, nBuilder.build())

    }
}
