package com.example.weather.services

import android.os.AsyncTask
import com.example.weather.AppSettings
import com.google.firebase.iid.FirebaseInstanceId
import org.json.JSONObject
import java.net.URL

data class WeatherResponse(var weather: String = "", var tempInC: String = "", var rain: String = "", var error: Exception? = null)

class WeatherService() {
    fun getWeatherForCity(city: String) : WeatherResponse {
        val weatherTask = WeatherTask()
        val jsonResponse = weatherTask.execute(city).get()
        return handleResponseForCity(jsonResponse)
    }

    private fun handleResponseForCity(jsonResponse: String?) : WeatherResponse{
        val response = WeatherResponse()
        try {
            val jsonObj = JSONObject(jsonResponse).getJSONObject("data")
            response.rain = "%.0f".format(jsonObj.getDouble("rainChance") * 100)
            response.tempInC = jsonObj.getString("temperature")
            response.weather = jsonObj.getString("weather")
        } catch (e: Exception) {
            response.error = e
        }
        return response
    }
}

class WeatherTask : AsyncTask<String, Void, String>(){
    override fun doInBackground(vararg params: String?): String? {
        return try {
            URL(AppSettings.API_URL + "/api/weather/${params[0]}")
                .readText(Charsets.UTF_8)
        } catch (e: Exception){
            null//TODO error propagation?
        }
    }
}
