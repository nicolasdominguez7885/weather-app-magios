package com.example.weather

class ImagesProvider {
    companion object {
        fun getImageAssets() : List<Int>{
            return listOf(R.drawable.sunny, R.drawable.cloudy, R.drawable.rainy, R.drawable.snowing)
        }

        fun getImage(weather: String) : Int {
            return when(weather) {
                "Rain", "Thunderstorm", "Drizzle" -> R.drawable.rainy
                "Snow" -> R.drawable.snowing
                "Clear" -> R.drawable.sunny
                "Clouds" -> R.drawable.cloudy
                else -> R.drawable.sunny
            }
        }
    }
}