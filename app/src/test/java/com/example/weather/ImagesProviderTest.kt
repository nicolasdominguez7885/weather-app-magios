package com.example.weather

import org.junit.Test

import org.junit.Assert.*

class ImagesProviderTest {
    @Test
    fun assert_rain_images() {
        assertEquals(R.drawable.rainy, ImagesProvider.getImage("Rain"));
        assertEquals(R.drawable.rainy, ImagesProvider.getImage("Thunderstorm"));
        assertEquals(R.drawable.rainy, ImagesProvider.getImage("Drizzle"));
    }

    @Test
    fun assert_cloud_images() {
        assertEquals(R.drawable.cloudy, ImagesProvider.getImage("Clouds"));
    }

    @Test
    fun assert_clear_images() {
        assertEquals(R.drawable.sunny, ImagesProvider.getImage("Clear"));
    }

    @Test
    fun assert_snow_images() {
        assertEquals(R.drawable.snowing, ImagesProvider.getImage("Snow"));
    }

    @Test
    fun assert_default_image() {
        assertEquals(R.drawable.sunny, ImagesProvider.getImage("OtherWeather"));
    }
}